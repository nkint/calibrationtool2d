#include "groundplane.h"
#include "ui_groundplane.h"

#include <iostream>
#include <vector>
#include <QFileDialog>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "utils.h"

GroundPlane::GroundPlane(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GroundPlane),
    first_frame(true),
    got_estrinsic(false)
{
    ui->setupUi(this);

    display4click = new ClickLabel();
    imitateGeometry(ui->frame_original, display4click);
    display4click->setParent(ui->frame_original);
    setQFrameBackgroundColor(ui->frame_original, QColor(0,0,0) );

    connect(display4click, SIGNAL(newClick(QPoint)),
            this, SLOT(checkPoints()));

    display3D = new GLView();
    imitateGeometry(ui->frame_gl, display3D);
    display3D->setParent(ui->frame_gl);
    setQFrameBackgroundColor(ui->frame_gl, QColor(0,0,0));

    connect(ui->slider_tx, SIGNAL(valueChanged(double)),
            this, SLOT(changeCameraX(double)));
    connect(ui->slider_ty, SIGNAL(valueChanged(double)),
            this, SLOT(changeCameraY(double)));
    connect(ui->slider_tz, SIGNAL(valueChanged(double)),
            this, SLOT(changeCameraZ(double)));

    connect(ui->slider_rx, SIGNAL(valueChanged(double)),
            this, SLOT(changeCameraAngleX(double)));
    connect(ui->slider_ry, SIGNAL(valueChanged(double)),
            this, SLOT(changeCameraAngleY(double)));
    connect(ui->slider_rz, SIGNAL(valueChanged(double)),
            this, SLOT(changeCameraAngleZ(double)));

    this->clickBehaviour = ESTIMATE_PLANE;
    ui->radio_estimate->setChecked(true);
    ui->radio_line->setChecked(false);
    connect(ui->radio_estimate, SIGNAL(toggled(bool)),
           this, SLOT(changeClickBehaviour()));
    connect(ui->radio_line, SIGNAL(toggled(bool)),
           this, SLOT(changeClickBehaviour()));

    connect(ui->btn_save, SIGNAL(clicked()),
            this, SLOT(saveCameraParametersToFile()));
    connect(ui->btn_load, SIGNAL(clicked()),
            this, SLOT(loadCameraParametersFromFile()));
}

GroundPlane::~GroundPlane()
{
    delete ui;
}

void GroundPlane::setIntrinsicMatrix(const cv::Mat cameraMatrix, const cv::Mat distCoeff)
{
    display3D->physicalCamera.setIntrinsic(cameraMatrix.clone(), distCoeff.clone());
    display3D->physicalCamera.setFrustumFromIntrinsic(1624, 1234, 1, 55);
}

void GroundPlane::newFrame(cv::Mat original, cv::Mat undistorted)
{
    QImage qi;
    if(clickBehaviour==ESTIMATE_PLANE) {
        qi = QImage(original.data, original.size().width, original.size().height, QImage::Format_RGB888);
    } else {
        qi = QImage(undistorted.data, undistorted.size().width, undistorted.size().height, QImage::Format_RGB888);
    }
    qi = qi.scaled(display4click->size(), Qt::KeepAspectRatio).rgbSwapped();
    display4click->setPixmap( QPixmap::fromImage(qi) );

    display3D->newFrame(undistorted);

    if(first_frame) {
        this->W = original.size().width;
        this->H = original.size().height;

        int d = display4click->width() - qi.width();
        QRect g = display4click->geometry();
        g.setX(d/2);
        display4click->setGeometry(g);
        first_frame = false;

        this->scaleFactor = (float)original.size().width / (float)qi.width();

        std::cout << "GroundPlane::newFrame new with: " << d << std::endl;

        display3D->setImageRatio(this->W, this->H);

        //display3D->resize(display3D->width()/scaleFactor, (display3D->height()*2)*scaleFactor);
        g = display3D->geometry();
        g.setX(d/2);
        //display3D->setGeometry(g);
    }

    _original = original.clone();
}

void GroundPlane::checkPoints()
{
    if(clickBehaviour==ESTIMATE_PLANE) {
        checkEstimatePoints();
        //display3D->people.clear();
    } else if(clickBehaviour==GET_PROJECTED_LINE) {
        checkLinePoints();
        //display3D->people.clear();
    }
}

void GroundPlane::checkEstimatePoints()
{
    std::vector<cv::Point2f> imagePoints;
    std::vector<cv::Point3f> worldPoint;

    // the trick: some known points.
    worldPoint.push_back(cv::Point3f(0,0,0));
    worldPoint.push_back(cv::Point3f(7,0,0));
    worldPoint.push_back(cv::Point3f(7,0,4));
    worldPoint.push_back(cv::Point3f(0,0,4));

    if(display4click->getClicks().size()==(int)worldPoint.size()) {
        if(got_estrinsic==false) {
            got_estrinsic = true;
        }
        std::cout << "estimate the plane!" << std::endl;

        for(int i=0; i<(int)display4click->getClicks().size(); i++) {
            QPoint p = display4click->getClicks()[i];
            imagePoints.push_back( cv::Point2f(p.x()*scaleFactor, p.y()*scaleFactor) );
            cv::circle(_original, imagePoints[i], 5, cv::Scalar(255,0,0), -1);
        }

        // dirty test for the click/scaleFactor accuracy..
        QWidget *w = new QWidget; QLabel *l = new QLabel;
        QImage qi = QImage(_original.data, _original.size().width, _original.size().height, QImage::Format_RGB888);
        l->setPixmap(QPixmap::fromImage(qi));
        l->setParent(w); w->show();

        display3D->physicalCamera.setTransformFromMappedPoints(worldPoint, imagePoints);

        // set the sliders!
//        this->ui->slider_rz->setValue( display3D->physicalCamera.angle[2] );
//        this->ui->slider_ry->setValue( display3D->physicalCamera.angle[1] );
//        this->ui->slider_rx->setValue( display3D->physicalCamera.angle[0] );

//        this->ui->slider_tx->setValue( display3D->physicalCamera.position[0] );
//        this->ui->slider_ty->setValue( display3D->physicalCamera.position[1] );
//        this->ui->slider_tz->setValue( display3D->physicalCamera.position[2] );
    } else {
        if(got_estrinsic==true) {
            got_estrinsic = false;
            display4click->clearPoints();
        }
    }
}

void GroundPlane::checkLinePoints()
{
    Ray3D r;
    Plane p;
    Vec3D foot;
    bool rayIntersectPlane=false;

    int numClick = display4click->getClicks().size();
    float xScreen = display4click->getClicks().at(numClick-1).x();
    float yScreen = display4click->getClicks().at(numClick-1).y();
    float w = display4click->pixmap()->width();
    float h = display4click->pixmap()->height();

//    std::vector<cv::Point2f> theMousePoint;
//    theMousePoint.push_back( cv::Point2f(xScreen, yScreen) );
//    std::vector<cv::Point2f> theUndistortedMousePoint;

//    std::cout << "----" << std::endl;
//    std::cout <<display3D->physicalCamera.getCameraMatrix() << std::endl << display3D->physicalCamera.getDistortionCoefficent() << std::endl;
//    std::cout << "----" << std::endl;

//    std::cout << "GroundPlane::checkLinePoints: before undistortion: "<<xScreen<<" "<<yScreen << std::endl;
//    cv::undistortPoints(theMousePoint, theUndistortedMousePoint,
//                        display3D->physicalCamera.getCameraMatrix(),
//                        display3D->physicalCamera.getDistortionCoefficent());

//    xScreen = theUndistortedMousePoint[0].x;
//    yScreen = theUndistortedMousePoint[0].y;

//    std::cout << "GroundPlane::checkLinePoints: after undistortion: "<<xScreen<<" "<<yScreen << std::endl;

    r = display3D->physicalCamera.unproject(xScreen, yScreen, w, h);
    p = Plane(Vec3D(7,0,0),
              Vec3D(0,0,0),
              Vec3D(7,0,4));

    rayIntersectPlane = p.getIntersectionWithRay(r, foot);
    if(rayIntersectPlane) {
        display3D->people.push_back(foot);

        cv::Mat _m = _original.clone();

        cv::Point2f projected = display3D->physicalCamera.project(foot, _m.size().width, _m.size().height);

        cv::circle(_m, projected, 5, cv::Scalar(0,255,0));


        QWidget *w = new QWidget;
        QLabel *l = new QLabel;
        QImage qi = QImage(_m.data, _m.size().width, _m.size().height, QImage::Format_RGB888);
        l->setPixmap(QPixmap::fromImage(qi));
        l->setParent(w);
        w->show();

    } else {
        std::cout << "ray does not intersect the plane, something should be wrong.. (the normals?)" << std::endl;
    }
}

void GroundPlane::changeCameraX(double n)
{
    display3D->setPhysicalCameraX(n);
    display3D->updateGL();
}

void GroundPlane::changeCameraY(double n)
{
    display3D->setPhysicalCameraY(n);
    display3D->updateGL();
}

void GroundPlane::changeCameraZ(double n)
{
    display3D->setPhysicalCameraZ(n);
    display3D->updateGL();
}

void GroundPlane::changeCameraAngleX(double n)
{
    display3D->setPhysicalCameraAngleX(n);
    display3D->updateGL();
}

void GroundPlane::changeCameraAngleY(double n)
{
    display3D->setPhysicalCameraAngleY(n);
    display3D->updateGL();
}

void GroundPlane::changeCameraAngleZ(double n)
{
    display3D->setPhysicalCameraAngleZ(n);
    display3D->updateGL();
}

void GroundPlane::changeClickBehaviour()
{
    if(ui->radio_estimate->isChecked()) {
        this->clickBehaviour = ESTIMATE_PLANE;
        display4click->clearPoints();
        display3D->people.clear();
    } if(ui->radio_line->isChecked()) {
        this->clickBehaviour = GET_PROJECTED_LINE;
        display4click->clearPoints();
        display3D->people.clear();
    }
}

void GroundPlane::saveCameraParametersToFile()
{
    QString qfilename = QFileDialog::getSaveFileName(
                                        this,
                                        tr("Save camera parameters"),
                                        "", // "/home/alberto/.."
                                        tr("Settings (*.ini)"));

    display3D->physicalCamera.saveToFile(qfilename.toStdString());
}

void GroundPlane::loadCameraParametersFromFile()
{
    QString qfilename = QFileDialog::getOpenFileName(
                                        this,
                                        tr("Load camera parameters"),
                                        "", // "/home/alberto/.."
                                        tr("Settings (*.ini)"));

    display3D->physicalCamera.loadFromFile(qfilename.toStdString());
    display3D->physicalCamera.setFrustumFromIntrinsic(1624, 1234, 1, 55);

    //std::tr1::regex rx("ello");

}
