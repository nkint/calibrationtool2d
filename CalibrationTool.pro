#-------------------------------------------------
#
# Project created by QtCreator 2013-09-02T11:31:37
#
#-------------------------------------------------
#QMAKE_CXXFLAGS = -std=c+0x

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CalibrationTool
TEMPLATE = app

linux{

    message("* Using settings for Linux.")

    INCLUDEPATH += /usr/local/include/opencv
    LIBS += -L/usr/local/lib/ \
    /usr/local/lib/libopencv_calib3d.so \
    /usr/local/lib/libopencv_contrib.so \
    /usr/local/lib/libopencv_core.so \
    /usr/local/lib/libopencv_features2d.so \
    /usr/local/lib/libopencv_flann.so \
    /usr/local/lib/libopencv_gpu.so \
    /usr/local/lib/libopencv_highgui.so \
    /usr/local/lib/libopencv_imgproc.so \
    /usr/local/lib/libopencv_legacy.so \
    /usr/local/lib/libopencv_ml.so \
    /usr/local/lib/libopencv_nonfree.so \
    /usr/local/lib/libopencv_objdetect.so \
    /usr/local/lib/libopencv_photo.so \
    /usr/local/lib/libopencv_stitching.so \
    /usr/local/lib/libopencv_superres.so \
    /usr/local/lib/libopencv_ts.so \
    /usr/local/lib/libopencv_video.so \
    /usr/local/lib/libopencv_videostab.so \
    -lGLU -lGL
}

macx {
    QMAKE_CFLAGS_X86_64 += -mmacosx-version-min=10.7
    QMAKE_CXXFLAGS_X86_64 = $$QMAKE_CFLAGS_X86_64

    message("* Using settings for Mac OS X.")

    INCLUDEPATH += /usr/local/include/opencv
    LIBS += -L/usr/local/lib/ \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_calib3d \
        -lopencv_imgproc \
        -framework GLUT -framework OpenGL -framework Cocoa
}

SOURCES += main.cpp\
        mainwindow.cpp \
    undistort.cpp \
    groundplane.cpp \
    clicklabel.cpp \
    glview.cpp \
    matrices.cpp \
    camera.cpp

HEADERS  += mainwindow.h \
    undistort.h \
    groundplane.h \
    utils.h \
    clicklabel.h \
    glview.h \
    matrices.h \
    vectors.h \
    camera.h \
    opengl_draws.h \
    geometry.h

FORMS    += mainwindow.ui \
    undistort.ui \
    groundplane.ui
