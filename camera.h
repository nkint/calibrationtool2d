#ifndef CAMERA_H
#define CAMERA_H

#include <string>
#include <opencv2/core/core.hpp>
#include "matrices.h"
#include "geometry.h"

class Camera
{
public:
    Camera();
    ~Camera();

    void saveToFile(std::string filename);
    void loadFromFile(std::string filename);

    void setIntrinsic(cv::Mat cameraMatrix, cv::Mat distCoeff);

    void setFrustum(float l, float r, float b, float t, float n, float f);
    void setFrustum(float fovY, float aspectRatio, float front, float back);
    void setFrustumFromIntrinsic(int screeWidth, int screenHeight, int near, int far);

    void setTransform(Matrix4 n);
    void setTransformFromMappedPoints(std::vector<cv::Point3f> worldPoints,
                                      std::vector<cv::Point2f> imagePoints);

    Ray3D unproject(float xScreen, float yScreen, float w, float h);
    Vec2D project(Vec3D pointInWorld, float w, float h);

    void computeViewMatrixFromPositionAndRotationVectors();
    float position[3];
    float angle[3];

    cv::Mat getCameraMatrix() { return cameraMatrix; }
    cv::Mat getDistortionCoefficent() { return distCoeff; }
    Matrix4 getProjectionMatrix() { return projectionMatrix; }
    Matrix4 getViewMatrix() { return viewMatrix; }
    Matrix4 getTransformationMatrix() { return transformationMatrix; }
    float getFovy() { return fovy; }
    float getAspectRatio() { return aspectRatio; }
    float getNear() { return near; }
    float getFar() { return far; }

    cv::Mat extractMatrixFromString(std::string contents, std::size_t i, cv::Size size);
private:
    cv::Mat cameraMatrix;
    cv::Mat distCoeff;
    bool gotIntinsic;
    bool gotEstrinsic;

    Matrix4 projectionMatrix; // projection matrix
    Matrix4 viewMatrix; // view matrix
    Matrix4 transformationMatrix; // where the camera is in the world space
                                  // (the inverse of the view matrix)

    float fovy;
    float aspectRatio;
    float screenWidth, screenHeight;
    float near;
    float far;

};

#endif // CAMERA_H
