#ifndef GLVIEW_H
#define GLVIEW_H

#include <QGLWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <opencv2/core/core.hpp>
#include "geometry.h"
#include "matrices.h"
#include "camera.h"

class GLView : public QGLWidget
{
    Q_OBJECT
public:
    explicit GLView(QWidget *parent = 0);
    ~GLView();
    
    void init();                                    // initialize OpenGL states
    void draw();

    void setPhysicalCameraX(float x)        { physicalCamera.position[0] = x; physicalCamera.computeViewMatrixFromPositionAndRotationVectors(); }
    void setPhysicalCameraY(float y)        { physicalCamera.position[1] = y; physicalCamera.computeViewMatrixFromPositionAndRotationVectors(); }
    void setPhysicalCameraZ(float z)        { physicalCamera.position[2] = z; physicalCamera.computeViewMatrixFromPositionAndRotationVectors(); }
    void setPhysicalCameraAngleX(float p)   { physicalCamera.angle[0] = p; physicalCamera.computeViewMatrixFromPositionAndRotationVectors(); }
    void setPhysicalCameraAngleY(float h)   { physicalCamera.angle[1] = h; physicalCamera.computeViewMatrixFromPositionAndRotationVectors(); }
    void setPhysicalCameraAngleZ(float r)   { physicalCamera.angle[2] = r; physicalCamera.computeViewMatrixFromPositionAndRotationVectors(); }

    void setImageRatio(int W, int H);
    void newFrame(cv::Mat original);

    Camera thirdpersonCamera;
    Camera physicalCamera;

    void drawScene();
    QImage m_GLFrame;

    GLfloat wcoord[3]; // = {0,0,0};// wx, wy, wz;// returned xyz coords
    GLfloat wcoord1[3]; // = {0,0,0};
    std::vector<Vec3D> people;

signals:
    

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

public slots:

private:
    // member functions
    void initLights();                              // add a white light ti scene

    void drawSub1();                                // draw upper window
    void drawSub2();                                // draw bottom window
    void drawGroundplane();
    void drawFrame();

    // members
    int windowWidth;
    int windowHeight;
    bool windowSizeChanged;
    int W;
    int H;
    float originalRatio;

    int mouseX;
    int mouseY;
    float mousePosition[3];
    float mouseAngle[3];
};

#endif // GLVIEW_H
