#ifndef UTILS_H
#define UTILS_H

#include <QWidget>
#include <QColor>
#include <QPalette>
#include <QRect>

//---------------------------------------------------------------- internal gui utils

static inline
void setQFrameBackgroundColor(QWidget *n, QColor c)
{
    QPalette palette = n->palette();
    palette.setColor( n->backgroundRole(), c );
    n->setPalette( palette );
    n->setAutoFillBackground( true );
}

static inline
void imitateGeometry(QWidget *src, QWidget *dst)
{
    QRect g;
    g.setWidth( src->geometry().width()-1 );
    g.setHeight( src->geometry().height()-1 );
    g.setX(1);
    g.setY(1);
    dst->setGeometry(g);
}

#endif // UTILS_H
