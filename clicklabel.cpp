#include "clicklabel.h"

#include <iostream>
#include <QPainter>

ClickLabel::ClickLabel(QWidget *parent) :
    QLabel(parent)
{
}

QStack<QPoint> ClickLabel::getClicks()
{
    return this->points;
}

void ClickLabel::clearPoints()
{
    this->points.clear();
}

void ClickLabel::paintEvent(QPaintEvent *ev)
{
    QLabel::paintEvent(ev);

    QPainter painter;
    painter.begin( this );
    painter.setPen(Qt::white);

    for(int i=0; i<(int)points.size(); i++) {
        painter.drawEllipse(points[i].x()-3, points[i].y()-3, 6, 6);
    }

    painter.end();
}

void ClickLabel::mousePressEvent(QMouseEvent *ev)
{
    points.append(ev->pos());
    emit newClick(ev->pos());
}
