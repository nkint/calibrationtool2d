#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <QLayout>
#include <QFile>
#include <QFileInfo>
#include <opencv2/imgproc/imgproc.hpp>

bool isVideo = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qRegisterMetaType<cv::Mat>("cv::Mat");
    //preset.setPath(QApplication::applicationDirPath()+ "/preset.ini");
    //preset.setIniCodec(QSettings::NativeFormat);

    // opencv
    //std::string filename = "/Users/alberto/Movies/dominiks/video_alberto/tagli_11-08-40/easy_and_small2.mp4";
    //std::string filename = "/home/michele/Projects/code/Chapter3_MarkerlessAR/src/PyramidPatternTest.bmp";
    //std::string filename = "/Users/alberto/Downloads/00000001_normalized_.jpg";
    std::string filename = "/Users/alberto/Downloads/code-master/Chapter3_MarkerlessAR-build/src/00000001.jpg";

    QFile sourcefile(QString::fromStdString(filename));
    if(!sourcefile.exists()) {
        std::cerr << "file " << filename << " does not exist" << std::endl;
        qApp->exit(1);
    }

    QString s = QFileInfo(sourcefile).completeSuffix();
    if( s=="mp4" || s=="avi" || s=="mov") {
        isVideo = true;
    }

    if(isVideo) {
        video.open(filename);
        W = video.get(CV_CAP_PROP_FRAME_WIDTH);
        H = video.get(CV_CAP_PROP_FRAME_HEIGHT);
        std::cout << "opened video: " << filename << std::endl
                  << "with size: " << W << "-" << H << std::endl;
    } else {
        original = cv::imread(filename);
        W = original.size().width;
        H = original.size().height;
        std::cout << "opened image: " << filename << std::endl
                  << "with size: " << W << "-" << H << std::endl;
    }

    ui->setupUi(this);

    undistort = new Undistort(&video, W, H);
    this->ui->tabWidget->addTab(undistort, QString("intrinsic"));

    groundplane = new GroundPlane();
    this->ui->tabWidget->addTab(groundplane, QString("estrinsic"));

    connect(undistort, SIGNAL(intrinsicMatrix(cv::Mat, cv::Mat)),
            groundplane, SLOT(setIntrinsicMatrix(cv::Mat, cv::Mat)));

    // let's go!
    renderTimer = new QTimer();
    connect(renderTimer, SIGNAL(timeout()),
            this, SLOT(newFrame()));
    renderTimer->start(80);
}

MainWindow::~MainWindow()
{
    if(video.isOpened()) {
        video.release();
    }
    renderTimer->stop();
    undistort->deleteLater();
    groundplane->deleteLater();

    delete ui;
}

void MainWindow::newFrame()
{
    cv::Mat original_with_borders;

    if(isVideo) {
        video >> original;
    }

    if(!original.empty()) {
        int b = 0;
        cv::copyMakeBorder(original, original_with_borders, b, b, b, b, cv::BORDER_CONSTANT, cv::Scalar(255, 255, 255));
        this->undistort->newFrame(original_with_borders);
        //this->groundplane->newFrame(undistort->getUndistorted());
        this->groundplane->newFrame(original_with_borders, undistort->getUndistorted());
    } else {
        if(isVideo) {
            std::cout << "video finished, seek to frame 0" << std::endl;
            video.set(CV_CAP_PROP_POS_FRAMES, 0);
        } else {
            std::cerr << "the image is empty somethig is broken with the image file.." << std::endl;
            qApp->exit(1);
        }
    }
}
