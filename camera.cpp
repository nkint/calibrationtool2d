#include "camera.h"

#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>

Camera::Camera() :
    gotIntinsic(false),
    gotEstrinsic(false)
{
    //projectionMatrix = cv::Mat::eye(4,4, CV_64F)
    projectionMatrix.identity();
    viewMatrix.identity();

    position[0] = 0;
    position[1] = 0;
    position[2] = 0;

    angle[0] = 0;
    angle[1] = 0;
    angle[2] = 0;

    fovy = 0;
    aspectRatio = 0;
    screenWidth = 0;
    screenHeight = 0;
    near = 0;
    far = 0;
}

Camera::~Camera()
{

}

void Camera::saveToFile(std::string filename)
{
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    if (!fs.isOpened()) {
        std::cerr << "Camera::saveToFile "<<filename<<" can not be opened.."<<std::endl;
    }

    if(gotIntinsic) {
        fs << "camera intrinsic" << this->cameraMatrix;
        fs << "distortion coeff" << this->distCoeff;
    } else {
        std::cerr << "Camera::saveToFile > do not have the camera intrinsic.. not saving it.." << std::endl;
    }
    if(gotEstrinsic) {
        cv::Mat _t = cv::Mat(4,4,CV_32FC1,(float *)this->transformationMatrix.get());
        fs << "camera transformation" << _t;
    } else {
        std::cerr << "Camera::saveToFile > do not have the camera exstringic.. not saving it.." << std::endl;
    }

    fs.release();
}

void Camera::loadFromFile(std::string filename)
{
    cv::FileStorage fs;
    fs.open(filename, cv::FileStorage::READ);

    if (!fs.isOpened()) {
        std::cerr << "Camera::loadFromFile "<<filename<<" can not be opened.."<<std::endl;
    }

    cv::Mat intrinsic, distCoeff, transformation;
    fs["camera intrinsic"] >> intrinsic;
    fs["distortion coeff"] >> distCoeff;
    fs["camera transformation"] >> transformation;

    this->setIntrinsic(intrinsic, distCoeff);
    Matrix4 t;
    float *_t = (float*)transformation.data;
    t.set(  _t[0],
            _t[1],
            _t[2],
            _t[3],
            _t[4],
            _t[5],
            _t[6],
            _t[7],
            _t[8],
            _t[9],
            _t[10],
            _t[11],
            _t[12],
            _t[13],
            _t[14],
            _t[15]);

    this->setTransform(_t);
}

void Camera::computeViewMatrixFromPositionAndRotationVectors() // from position and angle vectors
{
    // TODO: this stuff doesn't work
    viewMatrix.identity();
    viewMatrix.translate(-position[0], -position[1], -position[2]);
    viewMatrix.rotateX(-angle[0]);    // pitch
    viewMatrix.rotateY(-angle[1]);    // heading
    viewMatrix.rotateZ(-angle[2]);    // roll

    Matrix4 t = viewMatrix;
    t.invert();

    this->transformationMatrix = t;
}

void Camera::setIntrinsic(cv::Mat cameraMatrix, cv::Mat distCoeff)
{
    this->cameraMatrix = cameraMatrix;
    this->distCoeff = distCoeff;
    gotIntinsic = true;
}

void Camera::setFrustum(float l, float r, float b, float t, float n, float f)
{
    //std::cout << "Camera::setFrustum" << std::endl;
    projectionMatrix.identity();
    projectionMatrix[0]  =  2 * n / (r - l);
    projectionMatrix[2]  =  (r + l) / (r - l);
    projectionMatrix[5]  =  2 * n / (t - b);
    projectionMatrix[6]  =  (t + b) / (t - b);
    projectionMatrix[10] = -(f + n) / (f - n);
    projectionMatrix[11] = -(2 * f * n) / (f - n);
    projectionMatrix[14] = -1;
    projectionMatrix[15] =  0;
}

void Camera::setFrustum(float fovY, float aspectRatio, float front, float back)
{
    //std::cout << "Camera::setFrustum" << std::endl;
    float tangent = tanf(fovY/2 * DEG2RAD);   // tangent of half fovY
    float height = front * tangent;           // half height of near plane
    float width = height * aspectRatio;       // half width of near plane

    // params: left, right, bottom, top, near, far
    setFrustum(-width, width, -height, height, front, back);
}

void Camera::setFrustumFromIntrinsic(int screenWidth, int screenHeight, int near, int far)
{
    if(!gotIntinsic) {
        std::cerr << "Camera::setFrustumFromIntrinsic : intrinsic matrix is missing!" << std::endl;
        return;
    }
    this->screenWidth = screenWidth;
    this->screenHeight = screenHeight;
    this->aspectRatio = (float)screenWidth / (float)screenHeight;
    this->near = near;
    this->far = far;

    float fy = cameraMatrix.at<double>(1,1);
    fovy = 2 * atan(screenHeight / (2 * fy)) * RAD2DEG;

    setFrustum(fovy, aspectRatio, near, far);
}

// the camera position in the world space
void Camera::setTransform(Matrix4 n)
{
    this->transformationMatrix = n;

    Matrix4 view = Matrix4(n);
    view.invert();
    this->viewMatrix = view;

    this->position[0] = n[3];
    this->position[1] = n[7];
    this->position[2] = n[11];

    // TODO: this stuff does not works..
    float rx, ry, rz;
    rx = atan2(n[9], n[10]);
    ry = atan2(-n[8], sqrt( n[9]*n[9] + n[10]*n[10] ));
    rz = atan2(n[4], n[0]);
    this->angle[0] = rx * RAD2DEG;
    this->angle[1] = ry * RAD2DEG;
    this->angle[2] = rz * RAD2DEG;
    std::cout << "Camera::setTransform rotation: " <<  angle[0] <<" "<< angle[1] <<" "<< angle[2] << std::endl;
}

void Camera::setTransformFromMappedPoints(std::vector<cv::Point3f> worldPoints, std::vector<cv::Point2f> imagePoints)
{
    if(!gotIntinsic) {
        std::cerr << "Camera::setTransformFromMappedPoints : intrinsic matrix is missing!" << std::endl;
        return;
    }

    cv::Mat rvec = cv::Mat::ones(3, 1, CV_64FC1);
    cv::Mat tvec = cv::Mat::ones(3, 1, CV_64FC1);
    cv::solvePnP(worldPoints, imagePoints, cameraMatrix, distCoeff, rvec, tvec); //, false, CV_P3P);

    cv::Mat R;
    cv::Rodrigues(rvec, R); // R is 3x3

    R = R.t();  // rotation of inverse
    tvec = -R * tvec; // translation of inverse

    cv::Mat T(4, 4, R.type()); // T is 4x4
    T( cv::Range(0,3), cv::Range(0,3) ) = R * 1; // copies R into T
    T( cv::Range(0,3), cv::Range(3,4) ) = tvec * 1; // copies tvec into T
    // fill the last row of T (NOTE: depending on your types, use float or double)
    double *p = T.ptr<double>(3);
    p[0] = p[1] = p[2] = 0; p[3] = 1;

    //std::cout << "rotation after rodrigues:" << std::endl << R << std::endl;
    //std::cout << "translation vector:" << std::endl << tvec << std::endl;
    //std::cout << "pose3d:" << std::endl << T << std::endl;

    cv::Mat T_float;
    T.convertTo(T_float, CV_32F);
    Matrix4 estimatedPose((float*)T_float.data);

    Matrix4 rotation_offset; // some rotations are needed for match opengl and opencv frames
    rotation_offset.rotateY(180);
    estimatedPose = estimatedPose * rotation_offset;
    rotation_offset.identity();
    rotation_offset.rotateZ(180);
    estimatedPose = estimatedPose * rotation_offset;

    setTransform(estimatedPose);
    gotEstrinsic = true;
}

// need w and h because the xScreen,yScreen could come from other display then the viewport
Ray3D Camera::unproject(float xScreen, float yScreen, float w, float h)
{
    Vector4 world1, world2;
    Matrix4 pvMatrix, pvMatrixInverse; // projection view
    pvMatrix = projectionMatrix * viewMatrix;

    // calculate mouse x,y clip space coordinates [-1, 1]
    float x = (xScreen-w/2)/(w/2);
    float y = -(yScreen-h/2)/(h/2);
    if(x>1)x=1; if(x<-1)x=-1;
    if(y>1)y=1; if(y<-1)y=-1;
    //std::cout << "the click in the clip space is:" << x << " " << y << std::endl;

    pvMatrixInverse.set( pvMatrix.get() );
    pvMatrixInverse.invert();

    // convert clip space coordinates into world space
    world1.set(x, y, -1, 1);
    world1 = pvMatrixInverse * world1;
    world1 *= 1/world1[3];
    world2.set(x, y, 1, 1);
    world2 = pvMatrixInverse * world2;
    world2 *= 1/world2[3];
    //std::cout << world1[0] << " " << world1[1] << " " << world1[2] << std::endl;
    //std::cout << world2[0] << " " << world2[1] << " " << world2[2] << std::endl;


    Ray3D r = Ray3D(Vec3D(world1.x, world1.y, world1.z),
                    Vec3D(world2.x, world2.y, world2.z));
    return r;
}

Vec2D Camera::project(Vec3D pointInWorld, float w, float h)
{
    Vector4 input;
    input.x = pointInWorld.x;
    input.y = pointInWorld.y;
    input.z = pointInWorld.z;
    input.w = 1;

    Matrix4 model;
    model.identity();

    input = ( projectionMatrix * viewMatrix ) * input;

    input.x /= input.w;
    input.y /= input.w;
    input.z /= input.w;

//    std::cout << "Camera::project" << std::endl;
//    std::cout << "transforming the point " << pointInWorld << " to: " << input << std::endl;

    input.x = (input.x + 1.0f) / 2.0f * w;
    input.y = (1.0f - input.y) / 2.0f * h;

//    std::cout << "that is..:" << input << std::endl;

    return Vec2D(input.x, input.y);
}


// those methods are probb good but now not tested

//// set camera position and lookat direction
//void Camera::setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ)
//{
//    float forward[4];
//    float up[4];
//    float left[4];
//    float position[4];
//    float invLength;

//    // determine forward vector (direction reversed because it is camera)
//    forward[0] = posX - targetX;    // x
//    forward[1] = posY - targetY;    // y
//    forward[2] = posZ - targetZ;    // z
//    forward[3] = 0.0f;              // w
//    // normalize it without w-component
//    invLength = 1.0f / sqrtf(forward[0]*forward[0] + forward[1]*forward[1] + forward[2]*forward[2]);
//    forward[0] *= invLength;
//    forward[1] *= invLength;
//    forward[2] *= invLength;

//    // assume up direction is straight up
//    up[0] = 0.0f;   // x
//    up[1] = 1.0f;   // y
//    up[2] = 0.0f;   // z
//    up[3] = 0.0f;   // w

//    // compute left vector with cross product
//    left[0] = up[1]*forward[2] - up[2]*forward[1];  // x
//    left[1] = up[2]*forward[0] - up[0]*forward[2];  // y
//    left[2] = up[0]*forward[1] - up[1]*forward[0];  // z
//    left[3] = 1.0f;                                 // w

//    // re-compute orthogonal up vector
//    up[0] = forward[1]*left[2] - forward[2]*left[1];    // x
//    up[1] = forward[2]*left[0] - forward[0]*left[2];    // y
//    up[2] = forward[0]*left[1] - forward[1]*left[0];    // z
//    up[3] = 0.0f;                                       // w

//    // camera position
//    position[0] = -posX;
//    position[1] = -posY;
//    position[2] = -posZ;
//    position[3] = 1.0f;

//    // copy axis vectors to matrix
//    matrixView.identity();
//    matrixView.setColumn(0, left);
//    matrixView.setColumn(1, up);
//    matrixView.setColumn(2, forward);
//    matrixView.setColumn(3, position);
//}

//void Camera::setOrthoFrustum(float l, float r, float b, float t, float n, float f)
//{
//    matrixProjection.identity();
//    matrixProjection[0]  =  2 / (r - l);
//    matrixProjection[3]  =  -(r + l) / (r - l);
//    matrixProjection[5]  =  2 / (t - b);
//    matrixProjection[7]  =  -(t + b) / (t - b);
//    matrixProjection[10] = -2 / (f - n);
//    matrixProjection[11] = -(f + n) / (f - n);
//}
