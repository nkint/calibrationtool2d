#ifndef UNDISTORT_H
#define UNDISTORT_H

#include <QWidget>
#include <QLabel>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace Ui {
class Undistort;
}

class Undistort : public QWidget
{
    Q_OBJECT
    
public:
    explicit Undistort(cv::VideoCapture *video, int W, int H, QWidget *parent = 0);
    ~Undistort();

    const cv::Mat &getUndistorted();

signals:
    void intrinsicMatrix(cv::Mat n, cv::Mat m);

public slots:
    void newFrame(cv::Mat original);

private:
    Ui::Undistort *ui;

    bool first_frame;
    int W, H;
    cv::VideoCapture *video;
    cv::Mat undistorted;

    QLabel *displayOriginal;
    QLabel *displayUndistorted;

    bool redoUndistort;

    void connectSliders();
    double radialX, radialY, tangX, tangY, focalX, focalY, centerX, centerY;
    double min_radialX, min_radialY, min_tangX, min_tangY, min_focalX, min_focalY, min_centerX, min_centerY;
    double max_radialX, max_radialY, max_tangX, max_tangY, max_focalX, max_focalY, max_centerX, max_centerY;

    QString settingFilename;

private slots:
    void valueChangedRadialX(double n);
    void valueChangedRadialY(double n);
    void valueChangedTangX(double n);
    void valueChangedTangY(double n);
    void valueChangedFocalX(double n);
    void valueChangedFocalY(double n);
    void valueChangedCenterX(double n);
    void valueChangedCenterY(double n);

    void saveSettings();
    void saveAsSettings();
    void loadNewSettings();
    void resetSettings();
};

#endif // UNDISTORT_H
