#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QSettings>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "undistort.h"
#include "groundplane.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();    

private:
    Ui::MainWindow *ui;

    cv::VideoCapture video;
    cv::Mat original;
    int W,H;

    QTimer *renderTimer;

    Undistort *undistort;
    GroundPlane *groundplane;

    QSettings preset;

private slots:
    void newFrame();
};

#endif // MAINWINDOW_H
