#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <iostream>
#include <opencv2/core/core.hpp>
#include "matrices.h"
#include "vectors.h"

#define EPSILON	1.1920928955078125E-7f
#define DEG2RAD (3.141593f / 180)
#define RAD2DEG (180 / 3.141593f)

typedef cv::Point2f Vec2D;
typedef cv::Point3f Vec3D;

/**
 * Normalizes the vector so that its magnitude = 1.
 */
// TODO: - some in-place version like void normalizeSelf(Vec3D &n)
//       - normalizes the vector to the given length and not 1
static inline
Vec3D normalize(Vec3D n) {
    Vec3D ret = n;
    float mag = sqrt(n.x * n.x + n.y * n.y + n.z * n.z);
    if (mag > 0) {
        mag = 1.0f / mag;
        ret.x *= mag;
        ret.y *= mag;
        ret.z *= mag;
    }
    return ret;
}


enum INTERSECTION{ NONE, COLLINEAR, OVERLAP, PARALLEL, SINGLE_POINT };

class Ray3D {
public:
    Ray3D() { /*empty constructor*/ }

    /* the ray of origin p1, pointing to p2 */
    Ray3D(Vec3D p1, Vec3D p2) {
        origin = p1;
        direction = normalize( (p2 - p1) );
    }

    const Vec3D &getDirection() {
        return direction;
    }

    const Vec3D &getOrigin() {
        return origin;
    }

    Vec3D getPointAtDistance(float dist) {
        Vec3D ret = this->origin + (direction  * dist);
        return ret;
    }

private:
    Vec3D origin;
    Vec3D direction;

};

class Plane {
public:
    Plane() { /*empty constructor*/ }

    /* a plane can be constructed from 3 points laying on the plane */
    Plane(Vec3D a, Vec3D b, Vec3D c) {
        Vec3D centroid = (a + b + c) * (1.0f / 3.0f);
        this->p = centroid;
        this->normal = normalize( (a - c).cross( a - b ) );
    }

    const Vec3D &getPoint() {
        return this->p;
    }
    const Vec3D &getNormal() {
        return this->normal;
    }

    bool getIntersectionWithRay(Ray3D r, Vec3D &result) {
            float denom = normal.dot( r.getDirection() );
            if (denom > EPSILON) {
                float u = normal.dot( p - r.getOrigin() ) / denom;
                result = r.getPointAtDistance(u);
                return true;
            } else {
                return false;
            }
        }

private:
    Vec3D p;
    Vec3D normal;
};

static inline
INTERSECTION intersectionSegment2Plane(const Vec3D &L0, const Vec3D &L1, const Vec3D &N, const Vec3D &P0, Vec3D &res) {
    Vec3D L;
    double numerator, denominator;

    L = L1 - L0;
    numerator = N.dot(P0 - L0);
    denominator = N.dot(L);

    if( denominator==0 ) {
        // plane and segment are parallel
        if(numerator==0) {
            //std::cout << "intersectionSegment2Plane > OVERLAP" << std::endl;
            return OVERLAP;
        }
        else {
            //std::cout << "intersectionSegment2Plane > NONE" << std::endl;
            return NONE;
        }
    }

    double t = numerator/denominator;
    if(t>=0 && t<=1) {
        res = L0 + L*t;
        return SINGLE_POINT;
    }

    return NONE;
    // notes:
    //    t > 0 and t < 1  :           The intersection occurs between the two end points
    //    t = 0                 :           The intersection falls on the first end point
    //    t = 1                 :           Intersection falls on the second end point
    //    t > 1                 :           Intersection occurs beyond second end Point
    //    t < 0                 :           Intersection happens before 1st end point.
}

#endif // GEOMETRY_H


//Plane p;
//p = Plane(Vec3D(10, 12, 5),
//          Vec3D(23, 42, -6),
//          Vec3D(-10, -100, -300) );
//std::cout << p.getPoint() << std::endl;
//std::cout << p.getNormal() << std::endl;
///*
//    [7.66667, -15.3333, -100.333]
//    [0.924781, -0.372781, 0.0762486]
//*/

//Ray3D r = Ray3D(Vec3D(10, 0, -EPSILON),
//                Vec3D(-100, 0.1, -EPSILON));
//std::cout << r.getDirection() << std::endl;
//std::cout << r.getPointAtDistance(EPSILON)  << std::endl;
///*
//    [-1, 0.000909091, 0]
//    [10, 1.08372e-10, -1.19209e-07]
//*/

//Vec3D intersection;
//INTERSECTION i= p.getIntersectionWithRay(r, intersection);
//if(i==SINGLE_POINT) {
//    std::cout << intersection << std::endl;
//}else if(i==NONE) {
//    std::cout << "no intersection" << std::endl;
//}
//r = Ray3D(Vec3D(0,0,0), Vec3D(100,100,100));
//i= p.getIntersectionWithRay(r, intersection);
//if(i==SINGLE_POINT) {
//    std::cout << intersection << std::endl;
//}else if(i==NONE) {
//    std::cout << "no intersection" << std::endl;
//}
///*
//    no intersection
//    [8.20644, 8.20644, 8.20644]
//*/
