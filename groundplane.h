#ifndef GROUNDPLANE_H
#define GROUNDPLANE_H

#include <QWidget>
#include <QLabel>
#include <opencv2/core/core.hpp>
#include "clicklabel.h"
#include "glview.h"
#include "geometry.h"

#define ESTIMATE_PLANE 0
#define GET_PROJECTED_LINE 1

namespace Ui {
class GroundPlane;
}

class GroundPlane : public QWidget
{
    Q_OBJECT
    
public:
    explicit GroundPlane(QWidget *parent = 0);
    ~GroundPlane();
    
public slots:
    void setIntrinsicMatrix(const cv::Mat cameraMatrix, const cv::Mat distCoeff);
    void newFrame(cv::Mat original, cv::Mat undistorted);

private:
    Ui::GroundPlane *ui;
    ClickLabel *display4click;
    GLView *display3D;

    bool first_frame;
    float scaleFactor;

    int W, H;

    bool got_estrinsic;
    cv::Mat _original;

    int clickBehaviour;

private slots:
    void checkPoints();
    void checkEstimatePoints();
    void checkLinePoints();

    void changeCameraX(double n);
    void changeCameraY(double n);
    void changeCameraZ(double n);
    void changeCameraAngleX(double n);
    void changeCameraAngleY(double n);
    void changeCameraAngleZ(double n);

    void changeClickBehaviour();

    void saveCameraParametersToFile();
    void loadCameraParametersFromFile();
};

#endif // GROUNDPLANE_H
