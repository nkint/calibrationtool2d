#ifndef CLICKLABEL_H
#define CLICKLABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QStack>
#include <QPoint>

class ClickLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ClickLabel(QWidget *parent = 0);
    
    QStack<QPoint> getClicks();
    void clearPoints();

signals:
    void newClick(QPoint p);

protected:
    void paintEvent(QPaintEvent *ev);
    void mousePressEvent(QMouseEvent *ev);

public slots:
    
private:
    QStack<QPoint> points;

};

#endif // CLICKLABEL_H
