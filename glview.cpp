#include "glview.h"

#include <cmath>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include "matrices.h"
#include "opengl_draws.h"

// constants
const float FOV_Y = 60.0f;              // vertical FOV in degree
const float NEAR_PLANE = 3.0f;
const float FAR_PLANE = 165.0f;
const float CAMERA_ANGLE_X = 45.0f;     // pitch in degree
const float CAMERA_ANGLE_Y = -45.0f;    // heading in degree
const float CAMERA_DISTANCE = 25.0f;    // camera distance


GLView::GLView(QWidget *parent) :
    QGLWidget(parent),
    windowWidth(0), windowHeight(0),
    windowSizeChanged(false)
{
    mousePosition[0] = mousePosition[1] = 0;
    mousePosition[2] = -20;
    mouseAngle[0] = 0; //45;
    mouseAngle[1] = mouseAngle[2] = 0;

    wcoord[0] = wcoord[1] = wcoord[2] = 0;
    wcoord1[0] = wcoord1[1] = wcoord1[2] = 10;
}

GLView::~GLView()
{

}

void GLView::setImageRatio(int W, int H)
{
    this->W = W;
    this->H = H;
    this->originalRatio = (float)W / (float)H;
}

void GLView::newFrame(cv::Mat original)
{
    QImage qtFrame(original.data, original.size().width, original.size().height, original.step, QImage::Format_RGB888);
    qtFrame = qtFrame.rgbSwapped();

    m_GLFrame = QGLWidget::convertToGLFormat(qtFrame);
    this->updateGL();
}

void GLView::initializeGL()
{
    glShadeModel(GL_SMOOTH);                        // shading mathod: GL_SMOOTH or GL_FLAT
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);          // 4-byte pixel alignment

    //glEnable(GL_MULTISAMPLE);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);

    // enable/disable features
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glEnable(GL_SCISSOR_TEST);

    // track material ambient and diffuse from surface color, call it before glEnable(GL_COLOR_MATERIAL)
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glClearStencil(0);                              // clear stencil buffer
    glClearDepth(1.0f);                             // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);

    initLights();
}

void GLView::initLights()
{
    // set up light colors (ambient, diffuse, specular)
    GLfloat lightKa[] = {.0f, .0f, .0f, 1.0f};      // ambient light
    GLfloat lightKd[] = {.9f, .9f, .9f, 1.0f};      // diffuse light
    GLfloat lightKs[] = {1, 1, 1, 1};               // specular light
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);
    // position the light in eye space
    float lightPos[4] = {0, 1, 1, 0};               // directional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glEnable(GL_LIGHT0);
}

void GLView::paintGL()
{
    //std::cout << "GLView::paintGL()" << std::endl;
    drawSub1();
    drawSub2();

    // post frame
    if(windowSizeChanged)
    {
        glViewport(0, 0, windowWidth, windowHeight);
        windowSizeChanged = false;
    }

    // fill mode
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
}

void GLView::drawSub1()
{
    // set the view and scissor
    int halfHeight = windowHeight / 2;
    glScissor(0, halfHeight, windowWidth, halfHeight);
    glViewport(0, halfHeight, windowWidth, halfHeight);

    // clean screen
    glClearColor(0.2f, 0.2f, 0.2f, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    drawFrame();

    // use physical camera projection matrix (the frustum, the perspective etcetera)
    // as the opengl camera
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(physicalCamera.getProjectionMatrix().getTranspose());
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    {
        // the view matrix: how the camera is positioned in the world space

        glLoadMatrixf(physicalCamera.getViewMatrix().getTranspose());

        // equivalent call with old opengl command
        //glRotatef(-physicalCamera.angle[2], 0, 0, 1);
        //glRotatef(-physicalCamera.angle[1], 0, 1, 0);
        //glRotatef(-physicalCamera.angle[0], 1, 0, 0);
        //glTranslatef(-physicalCamera.position[0], -physicalCamera.position[1], -physicalCamera.position[2]);

        drawScene();
    }
    glPopMatrix();
}

void GLView::drawSub2()
{
    // viewport and scissor
    int halfHeight = windowHeight / 2;
    glScissor(0, 0, windowWidth, halfHeight);
    glViewport(0, 0, windowWidth, halfHeight);


    // set perspective viewing frustum in the 3rd person camera
    thirdpersonCamera.setFrustum(FOV_Y, (float)(windowWidth)/(windowHeight/2), NEAR_PLANE, FAR_PLANE); // FOV, AspectRatio, NearClip, FarClip

    // clear screen
    glClearColor(0, 0, 0, 1);   // background color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // use 3rd person camera projection matrix (the frustum, the perspective etcetera)
    // as the opengl camera
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(thirdpersonCamera.getProjectionMatrix().getTranspose());
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    {
        // First, transform the camera (viewing matrix) from world space to eye space
        glLoadMatrixf(thirdpersonCamera.getViewMatrix().getTranspose());

        // translate and rotate the mouse control
        // (keeping the camera fixed because
        // we want only the scene to rotate/translate)
        // for thinking about modern opengl: should be a matrix..
        glTranslatef(mousePosition[0], mousePosition[1], mousePosition[2]);
        glRotatef(mouseAngle[0], 1, 0, 0); // pitch
        glRotatef(mouseAngle[1], 0, 1, 0); // heading
        glRotatef(mouseAngle[2], 0, 0, 1); // roll

        // now the draw the scene
        drawScene();

        // draw the camera
        glPushMatrix();
        {
            glMultMatrixf(physicalCamera.getTransformationMatrix().getTranspose());

            drawAxis(4);
            drawCamera();
            drawFrustum(physicalCamera.getFovy(), physicalCamera.getAspectRatio(), physicalCamera.getNear(), physicalCamera.getFar());
        }
        glPopMatrix();

    }
    glPopMatrix();
}

void GLView::drawScene()
{
    drawAxis(4);
    drawGrid(10, 1);
    drawGroundplane();

    glPushMatrix();
    {
        for(int i=0; i<(int)people.size(); i++) {
            drawHumanBox(people[i]);
        }
    }
    glPopMatrix();
}

void GLView::drawGroundplane()
{
    // disable lighting
    glDisable(GL_LIGHTING);

    glBegin(GL_LINE_STRIP);
    glColor3f(1, 0.3f, 0.3f);
    glVertex3f(0,-0.1,0);
    glVertex3f(7,-0.1,0);
    glVertex3f(7,-0.1,4);
    glVertex3f(0,-0.1,4);
    glVertex3f(0,-0.1,0);
    glEnd();

    // enable lighting back
    glEnable(GL_LIGHTING);
}

void GLView::drawFrame()
{
    glDisable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, windowWidth, windowHeight/2, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //glTranslatef(0, -5, 0);

    glColor3f(1, 1, 1);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, m_GLFrame.width(), m_GLFrame.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, m_GLFrame.bits() );
    glBegin(GL_QUADS);
    int _w=windowWidth, _h=windowHeight/2;

    glTexCoord2f(0, 1); glVertex2f(0, 0);
    glTexCoord2f(0, 0); glVertex2f(0, _h);
    glTexCoord2f(1, 0); glVertex2f(_w, _h);
    glTexCoord2f(1, 1); glVertex2f(_w, 0);

    glEnd();
    glDisable(GL_TEXTURE_2D);

    glEnable(GL_DEPTH_TEST);
}

void GLView::resizeGL(int width, int height)
{
    windowWidth = width;
    windowHeight = height;
    windowSizeChanged = true;
}

void GLView::mousePressEvent(QMouseEvent *event)
{
    mouseX = event->pos().x();
    mouseY = event->pos().y();
    updateGL();
}

void GLView::mouseMoveEvent(QMouseEvent *event)
{
    float x = event->pos().x();
    float y = event->pos().y();
    mouseAngle[1] += (x - mouseX);
    mouseAngle[0] += (y - mouseY);
    mouseX = x;
    mouseY = y;
    updateGL();
}

void GLView::wheelEvent(QWheelEvent *event)
{
    mousePosition[2] -= event->delta();
    updateGL();
}
