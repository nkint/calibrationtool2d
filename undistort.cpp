#include "undistort.h"
#include "ui_undistort.h"

#include <iostream>
#include <QRect>
#include <QSettings>
#include <QFileDialog>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "utils.h"

Undistort::Undistort(cv::VideoCapture *video, int W, int H, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Undistort),
    first_frame(true),
    W(W),
    H(H),
    redoUndistort(true)
{
    this->video = video;

    // ui
    ui->setupUi(this);

    displayOriginal = new QLabel();
    imitateGeometry(ui->frame_original, displayOriginal);
    displayOriginal->setParent(ui->frame_original);
    setQFrameBackgroundColor(ui->frame_original, QColor(0,0,0) );

    displayUndistorted = new QLabel();
    imitateGeometry(ui->frame_undistorted, displayUndistorted);
    displayUndistorted->setParent(ui->frame_undistorted);
    setQFrameBackgroundColor(ui->frame_undistorted, QColor(0,0,0) );

    connectSliders();

    connect(ui->button_save, SIGNAL(clicked()),
            this, SLOT(saveSettings()));
    connect(ui->button_saveas, SIGNAL(clicked()),
            this, SLOT(saveAsSettings()));
    connect(ui->button_load, SIGNAL(clicked()),
            this, SLOT(loadNewSettings()));
    connect(ui->button_reset, SIGNAL(clicked()),
            this, SLOT(resetSettings()));

    min_radialX = -2; ui->slider_radialX->setMinimum(min_radialX);
    max_radialX =  2; ui->slider_radialX->setMaximum(max_radialX);
    min_radialY = -2; ui->slider_radialY->setMinimum(min_radialY);
    max_radialY =  2; ui->slider_radialY->setMaximum(max_radialY);
    min_tangX = -2; ui->slider_tangX->setMinimum(min_tangX);
    max_tangX =  2; ui->slider_tangX->setMaximum(max_tangX);
    min_tangY = -2; ui->slider_tangY->setMinimum(min_tangY);
    max_tangY =  2; ui->slider_tangY->setMaximum(max_tangY);
    min_focalX = -W*4; ui->slider_focalX->setMinimum(min_focalX);
    max_focalX =  W*4; ui->slider_focalX->setMaximum(max_focalX);
    min_focalY = -H*4; ui->slider_focalY->setMinimum(min_focalY);
    max_focalY =  H*4; ui->slider_focalY->setMaximum(max_focalY);
    min_centerX = 0; ui->slider_centerX->setMinimum(min_centerX);
    max_centerX = W; ui->slider_centerX->setMaximum(max_centerX);
    min_centerY = 0; ui->slider_centerY->setMinimum(min_centerY);
    max_centerY = H; ui->slider_centerY->setMaximum(max_centerY);

    // default values
    radialX = -0.48; ui->slider_radialX->setValue(radialX);
    radialY = -0.0; ui->slider_radialY->setValue(radialY);
    tangX = -0.0; ui->slider_tangX->setValue(tangX);
    tangY = -0.0; ui->slider_tangY->setValue(tangY);
    focalX = 1600; ui->slider_focalX->setValue(focalX);
    focalY = 1600; ui->slider_focalY->setValue(focalY);
    centerX = 789; ui->slider_centerX->setValue(centerX);
    centerY = 650; ui->slider_centerY->setValue(centerY);
}

Undistort::~Undistort()
{   
    undistorted.release();
    displayOriginal->deleteLater();
    displayUndistorted->deleteLater();
    delete ui;
}

const cv::Mat &Undistort::getUndistorted() {
    return undistorted;
}

void Undistort::newFrame(cv::Mat original)
{
//    std::cout << "Undistort::newFrame" << std::endl;
    undistorted = original.clone();
    double camIntrinsics[] = { focalX, 0, centerX, 0, focalY, centerY, 0, 0, 1 };
    double distortionCoeffs[] = { radialX, radialY, tangX, tangY };
    cv::Mat camera_matrix = cv::Mat(3,3,CV_64FC1,camIntrinsics);
    cv::Mat distortion_coefficients = cv::Mat(4,1,CV_64FC1,distortionCoeffs);

//    std::cout << "-----" << std::endl;
//    std::cout << camera_matrix;
//    std::cout << std::endl << std::endl;
//    std::cout << distortion_coefficients;
//    std::cout << std::endl << std::endl;

    cv::undistort(original, undistorted, camera_matrix, distortion_coefficients);

//    cv::Size new_image_size;
//    cv::Mat new_camera_matrix = cv::getOptimalNewCameraMatrix(camera_matrix, distortion_coefficients,
//                                                              original.size(), 1, new_image_size);

//    std::cerr << new_image_size.width << " " <<new_image_size.height << std::endl;

//    cv::Mat view, rview, map1, map2;
//    cv::initUndistortRectifyMap(camera_matrix, distortion_coefficients, cv::Mat(),
//                            new_camera_matrix,
//                            new_image_size, CV_16SC2, map1, map2);
//    view = original.clone();
//    cv::remap(original, undistorted, map1, map2, cv::INTER_LINEAR);


    QImage qi;
    qi = QImage(original.data, original.size().width, original.size().height, QImage::Format_RGB888);
    qi = qi.scaled(displayOriginal->size(), Qt::KeepAspectRatio).rgbSwapped();
    displayOriginal->setPixmap( QPixmap::fromImage(qi) );

    qi = QImage(undistorted.data, undistorted.size().width, undistorted.size().height, QImage::Format_RGB888);
    qi = qi.scaled(displayUndistorted->size(), Qt::KeepAspectRatio).rgbSwapped();
    displayUndistorted->setPixmap( QPixmap::fromImage(qi) );

    if(first_frame) {
        int d = displayOriginal->width() - qi.width();
        QRect g = displayOriginal->geometry();
        g.setX(d/2);
        displayOriginal->setGeometry(g);
        displayUndistorted->setGeometry(g);
        first_frame = false;
    }

    if(redoUndistort) {
        redoUndistort = false;
        emit intrinsicMatrix(camera_matrix, distortion_coefficients);
    }
}

//---------------------------------------------------------------- gui

void Undistort::connectSliders()
{
    connect(ui->slider_radialX, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedRadialX(double)));
    connect(ui->slider_radialY, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedRadialY(double)));

    connect(ui->slider_tangX, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedTangX(double)));
    connect(ui->slider_tangY, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedTangY(double)));

    connect(ui->slider_focalX, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedFocalX(double)));
    connect(ui->slider_focalY, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedFocalY(double)));

    connect(ui->slider_centerX, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedCenterX(double)));
    connect(ui->slider_centerY, SIGNAL(valueChanged(double)),
            this, SLOT(valueChangedCenterY(double)));
}

void Undistort::valueChangedRadialX(double n)
{
    //double v = remap(n, 0, 100, min_radialX, max_radialX);
    radialX = n;
    std::cout << "radial x: " << radialX << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedRadialY(double n)
{
    //double v = remap(n, 0, 100, min_radialY, max_radialY);
    radialY = n;
    std::cout << "radial y: " << radialY << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedTangX(double n)
{
    //double v = remap(n, 0, 100, min_tangX, max_tangX);
    tangX = n;
    std::cout << "tang x: " << tangX << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedTangY(double n)
{
    //double v = remap(n, 0, 100, min_tangY, max_tangY);
    tangY = n;
    std::cout << "tang y: " << tangY << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedFocalX(double n)
{
    //double v = remap(n, 0, 100, min_focalX, max_focalX);
    focalX = n;
    std::cout << "focal x: " << focalX << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedFocalY(double n)
{
    //double v = remap(n, 0, 100, min_focalY, max_focalY);
    focalY = n;
    std::cout << "focal y: " << focalY << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedCenterX(double n)
{
    //double v = remap(n, 0, 100, min_centerX, max_centerX);
    centerX = n;
    std::cout << "center x: " << centerX << std::endl;
    redoUndistort = true;
}

void Undistort::valueChangedCenterY(double n)
{
    //double v = remap(n, 0, 100, min_centerY, max_centerY);
    centerY = n;
    std::cout << "center y: " << centerY << std::endl;
    redoUndistort = true;
}

void Undistort::saveSettings()
{
    if(settingFilename.size()==0) {
        saveAsSettings();
    }

    QSettings preset(settingFilename, QSettings::NativeFormat);
    preset.setValue("radialX", QVariant(radialX));
    preset.setValue("radialY", QVariant(radialY));
    preset.setValue("tangX", QVariant(tangX));
    preset.setValue("tangY", QVariant(tangY));
    preset.setValue("focalX", QVariant(focalX));
    preset.setValue("focalY", QVariant(focalY));
    preset.setValue("centerX", QVariant(centerX));
    preset.setValue("centerY", QVariant(centerY));
}

void Undistort::saveAsSettings()
{
    settingFilename =
        QFileDialog::getSaveFileName(
            this,
            tr("Save file settings for distortions"),
            "", // "/home/alberto/.."
            tr("Settings (*.ini)"));

    saveSettings();
}

void Undistort::loadNewSettings()
{
    settingFilename =
            QFileDialog::getOpenFileName(
                this,
                tr("Load file settings for distortions"),
                "", // "/home/alberto/.."
                tr("Settings (*.ini)"));
    resetSettings();
}

void Undistort::resetSettings()
{
    if(settingFilename.size()==0) {
        loadNewSettings();
    }

    QSettings preset(settingFilename, QSettings::NativeFormat);
    radialX = preset.value("radialX").toDouble();
    radialY = preset.value("radialY").toDouble();
    tangX = preset.value("tangX").toDouble();
    tangY = preset.value("tangY").toDouble();
    focalX = preset.value("focalX").toDouble();
    focalY = preset.value("focalX").toDouble();
    centerX = preset.value("centerX").toDouble();
    centerY = preset.value("centerY").toDouble();

    ui->slider_radialX->setValue(radialX);
    ui->slider_radialY->setValue(radialY);
    ui->slider_tangX->setValue(tangX);
    ui->slider_tangY->setValue(tangY);
    ui->slider_focalX->setValue(focalX);
    ui->slider_focalY->setValue(focalY);
    ui->slider_centerX->setValue(centerX);
    ui->slider_centerY->setValue(centerY);

    redoUndistort = true;
}


